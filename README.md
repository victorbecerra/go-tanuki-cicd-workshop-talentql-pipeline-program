# Get more efficient with CI/CD: Introduction to GitLab - TalentQL Pipeline Program Workshop

This project is based on a template for CI/CD workshops and [documented in the Developer Evangelism handbook](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/projects/).

You'll learn 

* First Steps
* CI/CD
  * Terminology & First Steps
  * Pipeline Editor
  * Development Workflows with a Go Tanuki
* Security
* Delivery & Deploy
* What’s next
* Exercises for async practice

## Resources

- [Slides with exercises](https://docs.google.com/presentation/d/1xs4-mlf5L95aER_pVo6i5U0d5DW4f_AIG0RKk8SpKIc/edit)
- Solutions are provided in [.gitlab-ci.solution.yml](.gitlab-ci.solution.yml) and resp. commented code parts
- [Documentation](https://docs.gitlab.com/)
- [Website](https://about.gitlab.com/)
- [Handbook](https://about.gitlab.com/handbook/)
- Recording

[![](https://img.youtube.com/vi/id9klDUrGN8/0.jpg)](https://www.youtube.com/watch?v=id9klDUrGN8 "TalentQL Pipeline workshop in October 2021")

## My notes 🦊 

Edit this README.md with your own notes throughout the workshop 💡 

> **Tip**: [Markdown Reference](https://docs.gitlab.com/ee/user/markdown.html)

```yaml
this-is-yaml-in-a-code-block:
    script:
        - echo "Hello"
```

<!--
This is a hidden comment.

After the workshop, you can transfer this project into your personal namespace in `Settings > General > Advanced > Transfer project`.
-->
